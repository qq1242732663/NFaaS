#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"
#include "ns3/topology-read-module.h"
#include "model/ndn-l3-protocol.hpp"
#include "model/ndn-l3-protocol.hpp"
#include "helper/ndn-fib-helper.hpp"
#include "model/ndn-net-device-transport.hpp"
#include "model/ndn-global-router.hpp"

#define FREQUENCY "1"
#define TIME 2
#define CLOUD 1
namespace ns3 {

NS_LOG_COMPONENT_DEFINE("Simulation");


int main(int argc, char* argv[]) {
	// setting default parameters for PointToPoint links and channels
	Config::SetDefault("ns3::PointToPointNetDevice::DataRate",
			StringValue("500Mbps"));
	Config::SetDefault("ns3::PointToPointChannel::Delay", StringValue("10ms"));
	Config::SetDefault("ns3::DropTailQueue::MaxPackets", StringValue("50"));


	double time = TIME;
	string frequency = FREQUENCY;
	string size = "100";
	string deadline = "150";
	CommandLine cmd;

	cmd.AddValue("time", "Time of the simulation.", time);
	cmd.AddValue("frequency", "Number of kernels.", frequency);
	cmd.AddValue("size", "Task size", size);
	cmd.AddValue("deadline", "Task deadline", deadline);
	cmd.Parse(argc, argv);


	NodeContainer nodes;
	// Creating nodes
	nodes.Create(2);

	//TODO Set connections between nodes using p2p helper


	// Install NDN stack on all nodes
	ndn::StackHelper ndnHelper;
	ndnHelper.SetDefaultRoutes(true);
	ndnHelper.InstallAll();

	// Choosing forwarding strategy
	ndn::StrategyChoiceHelper::InstallAll("/",
			"/localhost/nfd/strategy/best-route2");
	ndn::StrategyChoiceHelper::InstallAll("/exec",
			"/localhost/nfd/strategy/function");

	// Install NDN applications
	std::string prefix_cloud = "/cloud/";
	std::string prefix_exec = "/exec/";

	// Configure Consumer
	// Consumer will generate requests to execute tasks
	ndn::AppHelper consumerHelper("ns3::ndn::ConsumerTask");
	consumerHelper.SetAttribute("Frequency", StringValue(frequency)); // 1 interests a second
	consumerHelper.SetAttribute("TaskSize", StringValue(size)); // 1 interests a second
	consumerHelper.SetAttribute("TaskDeadline", StringValue(deadline)); // 1 interests a second
	consumerHelper.SetAttribute("StartTime", StringValue("0s"));
	consumerHelper.SetAttribute("StopTime", StringValue("1s"));
	consumerHelper.SetAttribute("Randomize", StringValue("uniform"));

	//Install Consumer on Node 0
	consumerHelper.Install(nodes.Get(0));

	// Add /prefix-exec to all nodes
	// It means that they can execute tasks
	ndn::GlobalRoutingHelper ndnGlobalRoutingHelper;
	ndnGlobalRoutingHelper.InstallAll();
	for (ns3::Ptr<ns3::Node> node : nodes) {
		ndnGlobalRoutingHelper.AddOrigins(prefix_exec, node);
	}

	//Set one node as a gateway to the cloud
	//Tasks that cannot be executed locally, will be forwarded to this node
#ifdef CLOUD
	ndnGlobalRoutingHelper.AddOrigins(prefix_cloud, nodes.Get(CLOUD));
#endif

	// Calculate and install FIBs
	ndn::GlobalRoutingHelper::CalculateRoutes();

	// Set stop time for the whole simulation
	Simulator::Stop(Seconds(time));

	Simulator::Run();
	Simulator::Destroy();

	return 0;
}

} // namespace ns3

int main(int argc, char* argv[]) {
	return ns3::main(argc, argv);
}
