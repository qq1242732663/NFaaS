#ifndef NDN_CONSUMER_TASK_H
#define NDN_CONSUMER_TASK_H

#include "ns3/ndnSIM/model/ndn-common.hpp"

#include "ndn-consumer.hpp"

namespace ns3 {
namespace ndn {

/**
 * @ingroup ndn-apps
 * @brief Ndn application for sending out Interest packets at a "constant" rate (Poisson process)
 */
class ConsumerTask : public Consumer {
public:
  static TypeId
  GetTypeId();

  /**
   * \brief Default constructor
   * Sets up randomizer function and packet sequence number
   */
  ConsumerTask();
  virtual ~ConsumerTask();

protected:
  /**
   * \brief Constructs the Interest packet and sends it using a callback to the underlying NDN
   * protocol
   */
  virtual void
  ScheduleNextPacket();


protected:
  double m_frequency; // Frequency of interest packets (in hertz)
  bool m_firstTime;
  int m_taskSize;
  int m_taskDeadline;
};

} // namespace ndn
} // namespace ns3

#endif
