#include "kernel-info.hpp"
#include "ns3/core-module.h"
#include "ns3/network-module.h"


NFD_LOG_INIT("KernelInfo");

namespace nfd{
namespace ks{
KernelInfo::KernelInfo(Name name)
:m_name(name)
{
}

KernelInfo::KernelInfo(Name name, double deadline)
:m_name(name),
m_deadline(deadline)
{
}

KernelInfo::KernelInfo(const Interest interest)
: m_name(interest.getName()),
  m_deadline(interest.getTaskDeadline()),
  m_taskSize(interest.getTaskSize())
{
}

void
KernelInfo::updatePopularity(const unsigned long packetCounter, Name name, int hopCount) const 
{
 //   	NS_LOG_DEBUG("updatePopularity(" << packetCounter << ", " << name << ")" << "lastPacket: " << lastPacket << "periodSize: " << PERIOD_SIZE);

	if( (!(packetCounter % PERIOD_SIZE) || (popularity.size() == 0))  ){
//		NS_LOG_DEBUG("adding new row");
		lastPacket += PERIOD_SIZE;
		popularity.push_back(0);
		NS_LOG_DEBUG(*this);
		if(popularity.size() > POP_SIZE){
//			NS_LOG_DEBUG("popping");
			popularity.pop_front();
		}
	}
	
	if(name.equals(m_name)){
//		NFD_LOG_DEBUG("my name ++");
		popularity.back()++;
		m_hopCountSum += hopCount;
		if(++m_hopCountNum >= 10){
			m_hopCountSum /= 2;
			m_hopCountNum /= 2;
		}
		
	}else{
//		NFD_LOG_DEBUG("not my name "  << m_name << " " << name);
	}

}

void
KernelInfo::updateFacePopularity(FaceId faceId) const{
	NS_LOG_DEBUG(*this);
	std::list<FaceKernelInfo>::iterator fit;                                                
	std::list<FaceKernelInfo> tempList;                                     
	for(fit = m_prefFaces.begin(); fit != m_prefFaces.end(); fit++){                
		FaceKernelInfo faceInfo = *fit;                                                      		
		faceInfo.updatePopularity(m_packetCounter, faceId);                       
		tempList.push_back(faceInfo);                                         
	}                                                                                
	m_prefFaces.clear();                                                             	
	m_prefFaces = tempList;

}

void KernelInfo::updatePrefFaces(const unsigned long packetCounter, FaceId faceId){
	std::list<FaceKernelInfo>::const_iterator fit;
	std::list<FaceKernelInfo> tmpList;
	for(fit = m_prefFaces.begin(); fit != m_prefFaces.end(); fit++){
		FaceKernelInfo faceKernelInfo = *fit;
		faceKernelInfo.updatePopularity(packetCounter, faceId);
		tmpList.push_back(faceKernelInfo);
	}
	m_prefFaces.clear();
	m_prefFaces = tmpList;
}


double
KernelInfo::calculateScore() const{
  std::list<int>::reverse_iterator it;
  double score = 0.0;
  double weight = 10.0;
  for(it = popularity.rbegin(); it != popularity.rend(); it++){
    score += weight * (*it);
    weight -= 1.0;
  }
  double avrHopCount = m_hopCountSum / m_hopCountNum;
  if(m_taskType == 0){
	score += (3 - avrHopCount) * 10;
  }else{
	  score -= (3 - avrHopCount) * 10;

  }

  if(m_stored) score += 20;
  return score;
}
void
KernelInfo::updateDelay(unsigned int delay, FaceId faceId) const{
  NS_LOG_DEBUG("updating delay for face: " << faceId << "  delay:" << delay);	
  NS_LOG_DEBUG("Before update: " << *this);
  std::list<FaceKernelInfo>::const_iterator fit;
  for(fit = m_prefFaces.begin(); fit != m_prefFaces.end(); fit++){
    if(fit->getFaceId() == faceId){
	    FaceKernelInfo faceKernelInfo = *fit;
	    faceKernelInfo.updateDelay(delay);
	    m_prefFaces.erase(fit);
	    m_prefFaces.push_back(faceKernelInfo);
	    break;
    }
  }
  NS_LOG_DEBUG("After update: " << *this);
}


FaceId
KernelInfo::getNextFace() const{

//  NS_LOG_DEBUG("Before nextFace: " << *this);
	std::list<FaceKernelInfo>::iterator it;
	std::list<FaceKernelInfo> tmp;
	double minScore = 100000.0;
	FaceId bestFaceId = -1;
	for(it = m_prefFaces.begin(); it != m_prefFaces.end(); it++){
		//face overloaded
//		NS_LOG_DEBUG("Checking face " << it->getFaceId() << " avg_delay: " << it->getAverageDelay(), " m_deadline: " << m_deadline);
		FaceKernelInfo faceKernelInfo = *it;
		NS_LOG_DEBUG("~~~~~~~~~~~~~~~Checking face " << it->getFaceId() << "avg delay: " << it->getAverageDelay() << " m_deadline: " << m_deadline);
		if(it->getAverageDelay() > m_deadline){
			NS_LOG_DEBUG("###############Face overloaded " << it->getFaceId());
			faceKernelInfo.m_delayNum++;
			NS_LOG_DEBUG("~~~~~~~~~~~" << faceKernelInfo);
		}else{
			double value = it->calculateScore();
			if(value < minScore){
				minScore = value;
				bestFaceId = it->getFaceId();
			}
		}
		tmp.push_back(faceKernelInfo);	
	}
	
	m_prefFaces.clear();
	m_prefFaces = tmp;

	NS_LOG_DEBUG("After nextFace: " << *this);
	NS_LOG_DEBUG("=====================Returning best face: " << bestFaceId);	
	return bestFaceId;
}

void
KernelInfo::faceExpired(FaceId faceId) const{

  std::list<FaceKernelInfo>::iterator fit;
  for(fit = m_prefFaces.begin(); fit != m_prefFaces.end(); fit++){
    	if(fit->getFaceId() == faceId){
		FaceKernelInfo info = *fit;
		info.m_expired++;
		m_prefFaces.erase(fit);
		if(info.m_expired < 3){
			m_prefFaces.push_back(info);
		}
		break;
	}
  }
}

void
KernelInfo::addFace(FaceId faceId) const{
//  	NS_LOG_DEBUG("Before addingFace: " << *this);
	FaceKernelInfo faceInfo(faceId);

	std::list<FaceKernelInfo>::iterator it;
	for(it = m_prefFaces.begin(); it != m_prefFaces.end(); it++){
  //		NS_LOG_DEBUG("Comparing: " << faceId << " with " << it->getFaceId());
		if(faceId == it->getFaceId()){
			FaceKernelInfo info =*it;
			m_prefFaces.erase(it);
			info.m_expired = 0;
			m_prefFaces.push_back(info);
  //			NS_LOG_DEBUG("return");
		       	return;
		}
	}
	m_prefFaces.push_back(faceInfo);
	if(m_prefFaces.size() > 3){
		m_prefFaces.pop_front();
	}
  //	NS_LOG_DEBUG("After addingFace: " << *this);
}

std::ostream &operator<<(std::ostream &os, nfd::ks::KernelInfo m) {
  std::list<int>::reverse_iterator it;
  os << m.getName()  << " score: " << m.calculateScore() << " m_hopCountSum: " << m.m_hopCountSum << " m_hopCountNum: " <<  m.m_hopCountNum << " lastPacket:" << m.lastPacket << " ->";
  for (it = m.popularity.rbegin(); it != m.popularity.rend(); ++it) {
    os << *it << "/" << m.PERIOD_SIZE << "|";
  }
  std::list<FaceKernelInfo>::iterator fit;
  os  << " faces:";
  for (fit = m.m_prefFaces.begin(); fit != m.m_prefFaces.end(); fit++){
	os << " " << *fit;
  }
  return os;
}


}
}
