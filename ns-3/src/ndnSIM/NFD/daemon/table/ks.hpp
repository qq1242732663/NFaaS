#ifndef NFD_DAEMON_TABLE_KS_HPP
#define NFD_DAEMON_TABLE_KS_HPP

#include "kernel-info.hpp"
#include "face-info.hpp"
#include <ndn-cxx/face.hpp>
#include "ns3/ndnSIM/model/ndn-common.hpp"
#include "table/fib.hpp"
#include "ns3/ndnSIM/model/ndn-global-router.hpp"

namespace nfd {
namespace ks {


/*struct comp_kernels{
  bool operator() (Kernel k1, Kernel k2) const{
    return !k1.getName().equals(k2.getName());
  }
};*/

struct comp_kernel_infos{
  bool operator() (nfd::ks::KernelInfo k1, nfd::ks::KernelInfo k2) const{
    return k1.getName().toUri() > k2.getName().toUri();
  }
};

struct comp_face_infos{
  bool operator() (nfd::ks::FaceInfo f1, nfd::ks::FaceInfo f2) const{
    return f1.getFaceId() > f2.getFaceId();
  }
};
//bool myfunction (KernelInfo k1, KernelInfo k2);// { return (k1.calculateScore() > k2.calculateScore()); }

class Ks
{


private:
    static const unsigned int MAX_STORED_KERNELS = 9;
    unsigned int CURRENT_STORED_KERNELS = MAX_STORED_KERNELS;
    static const unsigned int MAX_RUN_KERNELS = 1;
public:
  Ks();
/*  bool
  runKernel(Kernel& kernel);*/
  typedef std::function<void(const Interest&, const Data& data)> HitCallback;
  typedef std::function<void(const Interest&)> MissCallback;
  void
  find(const Interest& interest, const HitCallback& hitCallback,
			          const MissCallback& missCallback);
  void
  sendData(const Interest& interest, const HitCallback& hitCallback);
  void
  manageFacePopularity(const Interest& interest, const int packetCounter, FaceId faceId);
  void
  managePopularity(const Interest& interest, const int packetCounter, int hopCount);
  void 
  manageDelay(const Name name, FaceId faceId, ns3::Time delay);
  void 
  manageDelay(const Name name, ns3::Time delay);
  void 
  updateStore();
  void
  requestKernel(const Name& kernelName);
  void
  managePrefixAdvertisement();
  FaceId 
  getBestFace(FaceId faceId, const Interest& interest);

  void
  finishAdd();

  void
  deleteFib();

  void
  calculateTables();
	  
  void 
  updateFace(FaceId faceId);
  bool
  faceOverloaded(FaceId faceId);

  void
  setCaching(bool caching);

  void
  setCloud(bool cloud);

void
setFib(Fib* fib){
	  m_fib = fib;
  }

  void 
  setGlobalRouter(ns3::Ptr<ns3::ndn::GlobalRouter> gr){
    m_gr = gr;
  }


  std::vector<KernelInfo> m_storedKernels;
  std::vector<KernelInfo> m_runKernels;
  std::set<KernelInfo, comp_kernel_infos> m_kernelInfos;
  std::set<FaceInfo, comp_face_infos> m_faceInfos;
  std::list<Interest> m_queue;

  unsigned int m_packetsWithoutDelay = 0;

  std::map<const ndn::RegisteredPrefixId*, ndn::Name> m_advertisedPrefix;

  void printFib();
  void printLocalPrefixes();


private:
  void createFace();
  bool advertisePrefix(Name name);
  void onLocalhostRegistrationSuccess(const ndn::Name& name);
  void registrationFailed(const ndn::Name& name);
  void stopAdvertisePrefix(const ndn::RegisteredPrefixId*, ndn::Name name);

  shared_ptr<ndn::Face> m_face;
  Fib*                m_fib;
  ns3::Ptr<ns3::ndn::GlobalRouter>  m_gr;
  bool m_cloud;
  bool m_caching;
};


std::ostream &operator<<(std::ostream &os, Ks const &k); 


} // namespace ks


} // namespace nfd

#endif // NFD_DAEMON_TABLE_KS_HPP
