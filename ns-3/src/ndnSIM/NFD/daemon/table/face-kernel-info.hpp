#ifndef NFD_DAEMON_FW_FACE_KERNEL_INFO_HPP
#define NFD_DAEMON_FW_FACE_KERNEL_INFO_HPP


#include "ns3/random-variable-stream.h"
#include "ns3/ndnSIM/model/ndn-common.hpp"


namespace nfd{
namespace ks{


class FaceKernelInfo {
public:


  FaceKernelInfo();
  FaceKernelInfo(FaceId faceId);
  void
  updateDelay(unsigned int delay);

  FaceId
  getFaceId()const {
	  return m_faceId;
  }
  double
  calculateScore() const;
  double
  getAverageDelay() const;
  void
  updatePopularity(const unsigned long packetCounter, FaceId faceId);

  FaceId m_faceId;
  unsigned int m_sumDelay = 0;
  unsigned int m_delayNum = 0;
  unsigned int m_load = 0;
  bool m_overloaded = false;
  std::list<int> m_popularity;
  unsigned long m_lastPacket = 0;
  static const unsigned int PERIOD_SIZE = 3;
  static const unsigned int POP_SIZE = 10;
  unsigned int m_expired = 0;

};

std::ostream &operator<<(std::ostream &os, nfd::ks::FaceKernelInfo m);

} //namespace fw
} //namespace nfd

#endif // NFD_DAEMON_FW_FACE_INFO_HPP
