#include "face-kernel-info.hpp"


namespace nfd{
NFD_LOG_INIT("FaceKernelInfo");
namespace ks{
FaceKernelInfo::FaceKernelInfo()
{
}

FaceKernelInfo::FaceKernelInfo(FaceId faceId)
{
  m_faceId = faceId;
}

void
FaceKernelInfo::updateDelay(unsigned int delay){
  m_sumDelay+= delay;
  m_delayNum++;
  if(m_delayNum >= 10){ m_sumDelay /= 2; m_delayNum /= 2;}

}

double
FaceKernelInfo::getAverageDelay() const{
	NS_LOG_DEBUG("m_sumDelay: " << m_sumDelay << "m_delayNum: " << m_delayNum);
	if(m_delayNum) return m_sumDelay / m_delayNum;
	return 0;
}

double
FaceKernelInfo::calculateScore() const{
  std::list<int>::const_reverse_iterator it;
  double score = 0.0;
  double weight = 10.0;
  for(it = m_popularity.rbegin(); it != m_popularity.rend(); it++){
    score += weight * (*it);
    weight -= 1.0;
  }
  return score;
}

void
FaceKernelInfo::updatePopularity(const unsigned long packetCounter, FaceId faceId)                            
{ 
NS_LOG_DEBUG("updatePopularity(" << packetCounter << ", " << faceId << ")" << "lastPacket: " << m_lastPacket << "periodSize: " << PERIOD_SIZE);
                                                                                                      
        if( (!(packetCounter % PERIOD_SIZE) || (m_popularity.size() == 0))  ){                                          
		NS_LOG_DEBUG("adding new row");                                                       
                m_lastPacket += PERIOD_SIZE;                                                           
                m_popularity.push_back(0);                                                              
                NS_LOG_DEBUG(*this);                                                                  
                if(m_popularity.size() > POP_SIZE){                                                     
                        NS_LOG_DEBUG("popping");                                                      
                        m_popularity.pop_front();
                }
        }                                                                                             
                                                                                                      
        if(faceId == m_faceId){                                                                      
                NFD_LOG_DEBUG("my id ++");                                                          
                m_popularity.back()++;                                                                  
        }else{
                NFD_LOG_DEBUG("not my id "  << m_faceId << " " << faceId);                              
        }
                                                                                                      
} 
  


std::ostream &operator<<(std::ostream &os, nfd::ks::FaceKernelInfo f) {
  os << "FaceId: " << f.m_faceId << " m_sumDelay: " << f.m_sumDelay << " m_delayNum: " << f.m_delayNum << " ->";
  std::list<int>::reverse_iterator rit; 
  for (rit = f.m_popularity.rbegin(); rit != f.m_popularity.rend(); ++rit) {         
    os << *rit << "/" << f.PERIOD_SIZE << "|";                                 
  }
  return os;
}


}
}
