#!/bin/bash

FILE=$1
MAX_ID=$2
POP=$3
PERIOD=$4

./process_score.sh $1 $2 > output.dat
echo "#!/usr/bin/gnuplot" > gnuplot.gp

echo "NAME=\"output\"" >> gnuplot.gp

echo "load 'style.gp'" >> gnuplot.gp

echo "set key top left" >> gnuplot.gp

echo "set title 'm=$POP, n=$PERIOD'" >> gnuplot.gp

echo "set xlabel \"Time[s]\"" >> gnuplot.gp
echo "set ylabel \"Score\"" >> gnuplot.gp

echo "set output NAME.\".pdf\"" >> gnuplot.gp

echo "plot \\" >> gnuplot.gp

for i in $(seq 1 $2)
do
	let j=$i+1
	echo "NAME.\".dat\" using 1:$j title '$i' with linespoints ls $i, \\" >> gnuplot.gp
done

chmod +x gnuplot.gp

./gnuplot.gp

#evince output.pdf
