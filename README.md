## Instalation
* `git clone git@gitlab.com:mharnen/NFaaS.git`
* `cd NFaaS/ns-3`
* `./waf configure --enable-examples`
* `./waf`
*  `./waf --run="ndn-function"`

## Implementation Overview
The implementation touches mainly an implementation of NFD present in ndnSIM.


### Kernel Store
The main part of the implementation consist of the Kernel Store (`/ns-3/src/ndnSIM/NFD/daemon/tablei/ks.cpp`). The module exposes an interface similar to a ContentStore, but is storing unikernel images instead of static data. When the Kernel Store receives a request, it consults it's local cache for requested unikernel. If it's found, the module will attempt to run the requested function (given there's enough resources defined by `MAX_RUN_KERNELS`). Furthermore, if `SPECIALIZE` flag is activated, the Kernel Store will dynamically modify the cache size - lower it when the node becomes overloaded and increases it when there are few requests. Further details can be found in the paper. 

If a node is configure as a cloud node, it will simulate an additional network delay before queuing a requests, but considers unlimited CPU resources.

### Statistic Objects
`/ns-3/src/ndnSIM/NFD/daemon/table/kernel-info.cpp`
`/ns-3/src/ndnSIM/NFD/daemon/table/kernel-info.cpp`
`/ns-3/src/ndnSIM/NFD/daemon/table/kernel-info.cpp`


## Eclipse integration
https://www.nsnam.org/wiki/HOWTO_configure_Eclipse_with_ns-3

## Scripts
* `run.sh` - run the simulation for different sets of parameters
